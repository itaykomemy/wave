package com.memy.business;

public interface ICallback {
	void onSuccess();
	void onError();
	void onFinally();
}
