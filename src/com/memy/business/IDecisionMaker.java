package com.memy.business;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public interface IDecisionMaker {

	public abstract void decide(int fingersCount) throws JSONRPC2SessionException;
	
}