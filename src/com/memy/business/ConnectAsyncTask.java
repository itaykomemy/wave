package com.memy.business;

import android.os.AsyncTask;

import com.memy.manager.XbmcManager;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public class ConnectAsyncTask extends AsyncTask<XbmcManager, Void, Boolean> {

	private ICallback callback = null;

	public ConnectAsyncTask() {
		super();
	}

	public ConnectAsyncTask(ICallback callback) {
		super();
		this.callback = callback;
	}

	@Override
	protected Boolean doInBackground(XbmcManager... manager) {
		try {
			manager[0].Ping();
			return true;
		} catch (JSONRPC2SessionException ex) {
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		if (callback == null)
			return;
		if (result)
			callback.onSuccess();
		else
			callback.onError();
		callback.onFinally();
	}

}
