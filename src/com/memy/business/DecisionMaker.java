package com.memy.business;

import com.memy.manager.IManager;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public class DecisionMaker implements IDecisionMaker {

	IManager xbmcManager;

	int consecutiveFrames = 0;
	int lastCount = 0;
	final int CONSECUTIVE_FRAMES_THRESHOLD = 4;

	public DecisionMaker(IManager xbmcManager) {
		super();
		this.xbmcManager = xbmcManager;
	}

	@Override
	public void decide(int fingersCount) throws JSONRPC2SessionException {
		if (lastCount == fingersCount) {
			consecutiveFrames++;
			if (consecutiveFrames == CONSECUTIVE_FRAMES_THRESHOLD) {
				// act
				switch (fingersCount) {
				case 2:
					xbmcManager.Play();
					break;
				case 5:
					xbmcManager.Pause();
					break;

				default:
					break;
				}
			}

		} else {
			consecutiveFrames = 0;
		}
		lastCount = fingersCount;
	}

}
