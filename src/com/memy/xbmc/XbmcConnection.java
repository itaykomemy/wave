package com.memy.xbmc;

import java.net.MalformedURLException;
import java.net.URL;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;
import com.thetransactioncompany.jsonrpc2.JSONRPC2Response;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2Session;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionOptions;

public class XbmcConnection {

	private JSONRPC2Session session;
	private URL url;
	private JSONRPC2SessionOptions options;

	public XbmcConnection(String host, int port) throws MalformedURLException {
		
		options = new JSONRPC2SessionOptions();
		options.setRequestContentType(JSONRPC2SessionOptions.DEFAULT_CONTENT_TYPE);
		url = new URL("http", host, port, "/jsonrpc");

		// Create new JSON-RPC 2.0 client session
		session = new JSONRPC2Session(url);
		session.setOptions(options);
	}

	public JSONRPC2Response send(JSONRPC2Request request) throws JSONRPC2SessionException {
		return session.send(request);
	}
	
	public boolean ping() throws JSONRPC2SessionException {
		session.send(new JSONRPC2Request("JSONRPC.Ping",1));
		return true;
	}
	
}
