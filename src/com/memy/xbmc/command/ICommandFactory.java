package com.memy.xbmc.command;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;

public interface ICommandFactory {
	JSONRPC2Request getPlayCommand();
	JSONRPC2Request getStopCommand();
	JSONRPC2Request getPauseCommand();
}

