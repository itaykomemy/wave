package com.memy.xbmc.command;

import java.util.Arrays;

import com.thetransactioncompany.jsonrpc2.JSONRPC2Request;

public class CommandFactory implements ICommandFactory {

	private String method = "Input.ExecuteAction";

	@Override
	public JSONRPC2Request getPlayCommand() {
		return new JSONRPC2Request(method, Arrays.asList((Object)"play"), 1);
	}

	@Override
	public JSONRPC2Request getStopCommand() {
		return new JSONRPC2Request(method, Arrays.asList((Object)"stop"), 1);
	}

	@Override
	public JSONRPC2Request getPauseCommand() {
		return new JSONRPC2Request(method, Arrays.asList((Object)"pause"), 1);
	}

}
