package com.memy.manager;

import java.net.MalformedURLException;

import com.memy.xbmc.XbmcConnection;
import com.memy.xbmc.command.CommandFactory;
import com.memy.xbmc.command.ICommandFactory;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public class XbmcManager implements IManager {

	private XbmcConnection connection;
	private ICommandFactory cmdFactory;

	public XbmcManager(String host, int port) throws MalformedURLException {
		super();
		connection = new XbmcConnection(host, port);
		cmdFactory = new CommandFactory();
	}

	@Override
	public void Play() throws JSONRPC2SessionException {
		connection.send(cmdFactory.getPlayCommand());
	}

	@Override
	public void Stop() throws JSONRPC2SessionException {
			connection.send(cmdFactory.getStopCommand());
	}

	@Override
	public void Pause() throws JSONRPC2SessionException {
			connection.send(cmdFactory.getPauseCommand());
	}

	public void Ping() throws JSONRPC2SessionException {
		connection.ping();
	}

}
