package com.memy.manager;

import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public interface IManager {

	void Play() throws JSONRPC2SessionException;

	void Stop() throws JSONRPC2SessionException;

	void Pause() throws JSONRPC2SessionException;

}
