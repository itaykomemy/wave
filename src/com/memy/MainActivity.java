package com.memy;

import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.memy.business.ConnectAsyncTask;
import com.memy.business.ICallback;
import com.memy.manager.XbmcManager;

public class MainActivity extends Activity {

	static String TAG = "MainActivity";

	Button startBtn;
	Button connectBtn;
	Button settingsBtn;
	
	TextView hostTextView;
	
	LinearLayout progressLayout;
	
	View connectionStatusView;
	GradientDrawable indicatorShape;
	
	Intent startSettingsIntent;
	Intent startDetectionActivityIntent;

	XbmcManager xbmcManager;

	String host;
	int port;
	boolean testingMode = false;
	
	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		setContentView(R.layout.activity_main);

		startBtn = (Button) findViewById(R.id.startBtn);
		connectBtn = (Button) findViewById(R.id.connectionBtn);
		settingsBtn = (Button) findViewById(R.id.settingsBtn);

		hostTextView = (TextView) findViewById(R.id.host);
		
		progressLayout = (LinearLayout) findViewById(R.id.progressLayout);
		connectionStatusView = (View) findViewById(R.id.connection_indicator);
		indicatorShape = (GradientDrawable) connectionStatusView.getBackground();
		indicatorShape.setColor(Color.RED);
		
		startSettingsIntent = new Intent(this, SettingsActivity.class);
		startDetectionActivityIntent = new Intent(this, HandDetectionActivity.class);

		// View setup
		progressLayout.setVisibility(View.INVISIBLE);
		
	};

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		host = sp.getString("xbmc_host", "127.0.0.1");
		
		String strPort = sp.getString("xbmc_port","80");
		try{
		port = Integer.parseInt(strPort, 10);
		}catch (Exception e) {
			
		}
		
		testingMode = sp.getBoolean("pref_allowTesting", false);
		
		
		if (host.equals("") || strPort.equals("")){
			hostTextView.setText("Please define a host");
		} else {
			hostTextView.setText("Host: " + host + ":" + port);	
		}
		startBtn.setEnabled(testingMode);
		startBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Start the capturing
				startActivity(startDetectionActivityIntent);
			}
		});

		settingsBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(startSettingsIntent);
				showDisconnected();
			}
		});

	};

	private boolean isConnected = false;
	
	private void showConnected() {	
		startBtn.setEnabled(true);
		connectBtn.setText("Connected!");
		connectBtn.setEnabled(false);
		indicatorShape.setColor(Color.GREEN);
		progressLayout.setVisibility(View.INVISIBLE);
	}
	
	private void showConnecting() {
		connectBtn.setEnabled(false);
		progressLayout.setVisibility(View.VISIBLE);
	}
	
	private void showDisconnected() {
		indicatorShape.setColor(Color.RED);
		connectBtn.setText("Connect");
		connectBtn.setEnabled(true);
		startBtn.setEnabled(testingMode);
		progressLayout.setVisibility(View.INVISIBLE);
	}
	
	public void onConnectClick(View view) {
		if (isConnected) {
			isConnected = false;
			showConnected();
			return;
		}
		showConnecting();
		try {
			xbmcManager = new XbmcManager(host, port);
			Log.d(TAG, "Creating XBMC manager with " + host + ":" + Integer.toString(port));
		} catch (MalformedURLException e) {
			Toast.makeText(MainActivity.this, "Bad Url", Toast.LENGTH_LONG).show();
			showDisconnected();
			return;
		}

		ICallback callback = new ICallback() {

			@Override
			public void onSuccess() {
				Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_LONG).show();
				showConnected();
				isConnected = true;
			}

			@Override
			public void onError() {
				Toast.makeText(MainActivity.this, "Not Connected", Toast.LENGTH_LONG).show();
				showDisconnected();
				isConnected = false;
			}
			
			@Override
			public void onFinally() {
				
			}
		};

		ConnectAsyncTask connectTask = new ConnectAsyncTask(callback);
		connectTask.execute(xbmcManager);
		Log.d(TAG, "Attempting connect");
	}
}
