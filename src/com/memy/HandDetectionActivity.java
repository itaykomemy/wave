package com.memy;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractorMOG2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.memy.business.DecisionMaker;
import com.memy.business.IDecisionMaker;
import com.memy.manager.IManager;
import com.memy.manager.StaleManager;
import com.memy.manager.XbmcManager;
import com.thetransactioncompany.jsonrpc2.client.JSONRPC2SessionException;

public class HandDetectionActivity extends Activity implements CvCameraViewListener2 {

	// Just for debugging
	private static final String TAG = "HandGestureApp";

	// stores detection method: MOG2 or skin color
	public String detectionMethod;
	private final String DETECTION_METHOD_MOG = "MOG";

	private IManager xbmcManager;
	private IDecisionMaker decisionMaker;

	// Color Space used for hand segmentation
	private static final int COLOR_SPACE = Imgproc.COLOR_RGB2Lab;

	// public final Object sync = new Object();

	// Mode that presamples hand colors
	public static final int SAMPLE_MODE = 0;

	// Mode that generates binary image
	public static final int DETECTION_MODE = 1;

	// Mode that displays color image together with contours, fingertips,
	// defect points and so on.
	public static final int ACTIVE_MODE = 2;

	// Mode that presamples background colors
	public static final int BACKGROUND_MODE = 3;

	private WaveCameraView mWaveCameraView;

	// Initial mode is BACKGROUND_MODE to presample the colors of the hand
	private int mode = BACKGROUND_MODE;

	private BackgroundSubtractorMOG2 bgSubtractor;

	private static final int SAMPLE_NUM = 7;

	private Point[][] samplePoints = null;
	private double[][] avgColor = null;
	private double[][] avgBackColor = null;

	private ArrayList<ArrayList<Double>> averChans = new ArrayList<ArrayList<Double>>();

	private double[][] cLower = new double[SAMPLE_NUM][3];
	private double[][] cUpper = new double[SAMPLE_NUM][3];
	private double[][] cBackLower = new double[SAMPLE_NUM][3];
	private double[][] cBackUpper = new double[SAMPLE_NUM][3];

	private Scalar lowerBound = new Scalar(0, 0, 0);
	private Scalar upperBound = new Scalar(0, 0, 0);
	private int squareLen;

	private Mat sampleColorMat = null;
	private List<Mat> sampleColorMats = null;

	private Mat[] sampleMats = null;

	private Mat rgbaMat = null;

	private Mat rgbMat = null;
	private Mat bgrMat = null;

	private Mat interMat = null;

	private Mat binMat = null;
	private Mat binTmpMat = null;
	private Mat binTmpMat2 = null;
	private Mat binTmpMat0 = null;
	private Mat binTmpMat3 = null;

	private Mat tmpMat = null;
	private Mat backMat = null;
	private Mat difMat = null;
	private Mat binDifMat = null;

	private Scalar mColorsRGB[] = null;

	// Stores all the information about the hand
	private HandGesture hg = null;

	private int imgNum;
	// private int gesFrameCount;
	private int curLabel = 0;

	// Stores string representation of features to be written to train_data.txt
	// private ArrayList<String> feaStrs = new ArrayList<String>();

	File sdCardDir = Environment.getExternalStorageDirectory();
	File sdFile = new File(sdCardDir, "AppMap.txt");

	private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
		@Override
		public void onManagerConnected(int status) {
			switch (status) {
			case LoaderCallbackInterface.SUCCESS: {
				Log.i("Android Tutorial", "OpenCV loaded successfully");

				try {
					// System.loadLibrary("signal");
					System.loadLibrary("HandGestureAppp");
				} catch (UnsatisfiedLinkError ule) {
					Log.e(TAG, "Hey, could not load native library signal");
					Toast.makeText(getApplicationContext(), "Failed to load native library", Toast.LENGTH_LONG).show();
				}

//				if (detectionMethod == DETECTION_METHOD_MOG) {
					bgSubtractor = new BackgroundSubtractorMOG2();
					bgSubtractor.setInt("nmixtures", 3);
					bgSubtractor.setBool("detectShadows", false);
//				}
				mWaveCameraView.enableView();

			}
				break;
			default: {
				super.onManagerConnected(status);

			}
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
		String host = sp.getString("xbmc_host", "");
		int port = Integer.parseInt(sp.getString("xbmc_port", ""), 10);
		detectionMethod = sp.getString("pref_bgMethod", "Skin Color");
		boolean testMode = sp.getBoolean("pref_allowTesting", false);

		try {
			xbmcManager = testMode ? new StaleManager() : new XbmcManager(host, port);
		} catch (Exception e) {

		}

		decisionMaker = new DecisionMaker(xbmcManager);

		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_hand_detection);

		mWaveCameraView = (WaveCameraView) findViewById(R.id.HandGestureApp);
		mWaveCameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT);
		mWaveCameraView.setVisibility(SurfaceView.VISIBLE);
		mWaveCameraView.setCvCameraViewListener(this);
		mWaveCameraView.setMaxFrameSize(320, 240);

		mWaveCameraView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String toastStr = null;
				if (mode == BACKGROUND_MODE) {
					toastStr = "Background sampled!";
					rgbaMat.copyTo(backMat);
					mode = detectionMethod.equals(DETECTION_METHOD_MOG) ? DETECTION_MODE : SAMPLE_MODE;
				} else if (mode == SAMPLE_MODE) {
					mode = DETECTION_MODE;
					toastStr = "Sampling Finished!";
				} else if (mode == DETECTION_MODE) {
					mode = ACTIVE_MODE;
					toastStr = "Binary Display Finished!";

				} else if (mode == ACTIVE_MODE) {
					// Do nothing
					toastStr = "Detection Active";
				}

				Toast.makeText(getApplicationContext(), toastStr, Toast.LENGTH_LONG).show();
			}
		});
		
		samplePoints = new Point[SAMPLE_NUM][2];
		for (int i = 0; i < SAMPLE_NUM; i++) {
			for (int j = 0; j < 2; j++) {
				samplePoints[i][j] = new Point();
			}
		}

		avgColor = new double[SAMPLE_NUM][3];
		avgBackColor = new double[SAMPLE_NUM][3];

		for (int i = 0; i < 3; i++)
			averChans.add(new ArrayList<Double>());

		initCLowerUpper(50, 50, 10, 10, 10, 10);
		initCBackLowerUpper(50, 50, 3, 3, 3, 3);

		SharedPreferences numbers = getSharedPreferences("Numbers", 0);
		imgNum = numbers.getInt("imgNum", 0);



		Log.i(TAG, "Created!");
	}

	// Just initialize boundaries of the first sample
	void initCLowerUpper(double cl1, double cu1, double cl2, double cu2, double cl3, double cu3) {
		cLower[0][0] = cl1;
		cUpper[0][0] = cu1;
		cLower[0][1] = cl2;
		cUpper[0][1] = cu2;
		cLower[0][2] = cl3;
		cUpper[0][2] = cu3;
	}

	void initCBackLowerUpper(double cl1, double cu1, double cl2, double cu2, double cl3, double cu3) {
		cBackLower[0][0] = cl1;
		cBackUpper[0][0] = cu1;
		cBackLower[0][1] = cl2;
		cBackUpper[0][1] = cu2;
		cBackLower[0][2] = cl3;
		cBackUpper[0][2] = cu3;
	}

	@Override
	public void onCameraViewStarted(int width, int height) {
		Log.i(TAG, "On cameraview started!");

		if (sampleColorMat == null)
			sampleColorMat = new Mat();

		if (sampleColorMats == null)
			sampleColorMats = new ArrayList<Mat>();

		if (sampleMats == null) {
			sampleMats = new Mat[SAMPLE_NUM];
			for (int i = 0; i < SAMPLE_NUM; i++)
				sampleMats[i] = new Mat();
		}

		if (rgbMat == null)
			rgbMat = new Mat();

		if (bgrMat == null)
			bgrMat = new Mat();

		if (interMat == null)
			interMat = new Mat();

		if (binMat == null)
			binMat = new Mat();

		if (binTmpMat == null)
			binTmpMat = new Mat();

		if (binTmpMat2 == null)
			binTmpMat2 = new Mat();

		if (binTmpMat0 == null)
			binTmpMat0 = new Mat();

		if (binTmpMat3 == null)
			binTmpMat3 = new Mat();

		if (tmpMat == null)
			tmpMat = new Mat();

		if (backMat == null)
			backMat = new Mat();

		if (difMat == null)
			difMat = new Mat();

		if (binDifMat == null)
			binDifMat = new Mat();

		if (hg == null)
			hg = new HandGesture();

		mColorsRGB = new Scalar[] { new Scalar(255, 0, 0, 255), new Scalar(0, 255, 0, 255), new Scalar(0, 0, 255, 255) };
		
		mWaveCameraView.setupCamera();
	}

	@Override
	public void onCameraViewStopped() {
		Log.i(TAG, "On cameraview stopped!");
		// releaseCVMats();
	}

	// Called when each frame data gets received
	// inputFrame contains the data for each frame
	// Mode flow: BACKGROUND_MODE --> SAMPLE_MODE --> DETECTION_MODE <-->
	// TRAIN_REC_MODE
	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		
		rgbaMat = inputFrame.rgba();
		
		Core.flip(rgbaMat, rgbaMat, 1);

		Imgproc.GaussianBlur(rgbaMat, rgbaMat, new Size(5, 5), 5, 5);

		Imgproc.cvtColor(rgbaMat, rgbMat, Imgproc.COLOR_RGBA2RGB);

		// Convert original RGB colorspace to the colorspace indicated by
		// COLR_SPACE
		Imgproc.cvtColor(rgbaMat, interMat, COLOR_SPACE);

		if (mode == BACKGROUND_MODE) { // First mode which presamples
			// background colors
			if (detectionMethod.equals(DETECTION_METHOD_MOG)) {
				bgSubtractor.apply(rgbaMat, interMat, 0.1);
			} else {
				preSampleBack(rgbaMat);
			}
		} else if (mode == SAMPLE_MODE) {
			// Second mode which presamples the colors of the hand - when skin
			// color is selected
			preSampleHand(rgbaMat);

		} else if (mode == DETECTION_MODE) {
			// Third mode which generates the binary image containing the hand
			if (detectionMethod.equals(DETECTION_METHOD_MOG)) {
				bgSubtractor.apply(rgbaMat, binMat, 0.001);
			} else {
				produceBinImg(interMat, binMat);
			}

			return binMat;

		} else if (mode == ACTIVE_MODE) {

			if (detectionMethod.equals(DETECTION_METHOD_MOG)) {
				bgSubtractor.apply(rgbaMat, binMat, 0.02);
			} else {
				produceBinImg(interMat, binMat);
			}

			makeContours();

			int fingersUp = hg.featureExtraction(rgbaMat, curLabel);

			try {
				decisionMaker.decide(fingersUp);
			} catch (JSONRPC2SessionException e) {
				
			}
		}

		return rgbaMat;
	}

	// Presampling hand colors.
	// Output is avgColor, which is essentially a 7 by 3 matrix storing the
	// colors sampled by seven squares
	void preSampleHand(Mat img) {
		int cols = img.cols();
		int rows = img.rows();
		squareLen = rows / 20;
		Scalar color = mColorsRGB[2]; // Blue Outline

		samplePoints[0][0].x = cols / 2;
		samplePoints[0][0].y = rows / 4;
		samplePoints[1][0].x = cols * 5 / 12;
		samplePoints[1][0].y = rows * 5 / 12;
		samplePoints[2][0].x = cols * 7 / 12;
		samplePoints[2][0].y = rows * 5 / 12;
		samplePoints[3][0].x = cols / 2;
		samplePoints[3][0].y = rows * 7 / 12;
		samplePoints[4][0].x = cols / 1.5;
		samplePoints[4][0].y = rows * 7 / 12;
		samplePoints[5][0].x = cols * 4 / 9;
		samplePoints[5][0].y = rows * 3 / 4;
		samplePoints[6][0].x = cols * 5 / 9;
		samplePoints[6][0].y = rows * 3 / 4;

		for (int i = 0; i < SAMPLE_NUM; i++) {
			samplePoints[i][1].x = samplePoints[i][0].x + squareLen;
			samplePoints[i][1].y = samplePoints[i][0].y + squareLen;
		}

		for (int i = 0; i < SAMPLE_NUM; i++) {
			Core.rectangle(img, samplePoints[i][0], samplePoints[i][1], color, 1);
		}

		for (int i = 0; i < SAMPLE_NUM; i++) {
			for (int j = 0; j < 3; j++) {
				avgColor[i][j] = (interMat.get((int) (samplePoints[i][0].y + squareLen / 2),
						(int) (samplePoints[i][0].x + squareLen / 2)))[j];
			}
		}
	}

	// Presampling background colors.
	// Output is avgBackColor, which is essentially a 7 by 3 matrix storing the
	// colors sampled by seven squares
	void preSampleBack(Mat img) {
		int cols = img.cols();
		int rows = img.rows();
		squareLen = rows / 20;
		Scalar color = mColorsRGB[2]; // Blue Outline

		samplePoints[0][0].x = cols / 6;
		samplePoints[0][0].y = rows / 3;
		samplePoints[1][0].x = cols / 6;
		samplePoints[1][0].y = rows * 2 / 3;
		samplePoints[2][0].x = cols / 2;
		samplePoints[2][0].y = rows / 6;
		samplePoints[3][0].x = cols / 2;
		samplePoints[3][0].y = rows / 2;
		samplePoints[4][0].x = cols / 2;
		samplePoints[4][0].y = rows * 5 / 6;
		samplePoints[5][0].x = cols * 5 / 6;
		samplePoints[5][0].y = rows / 3;
		samplePoints[6][0].x = cols * 5 / 6;
		samplePoints[6][0].y = rows * 2 / 3;

		for (int i = 0; i < SAMPLE_NUM; i++) {
			samplePoints[i][1].x = samplePoints[i][0].x + squareLen;
			samplePoints[i][1].y = samplePoints[i][0].y + squareLen;
		}

		for (int i = 0; i < SAMPLE_NUM; i++) {
			Core.rectangle(img, samplePoints[i][0], samplePoints[i][1], color, 1);
		}

		for (int i = 0; i < SAMPLE_NUM; i++) {
			for (int j = 0; j < 3; j++) {
				avgBackColor[i][j] = (interMat.get((int) (samplePoints[i][0].y + squareLen / 2),
						(int) (samplePoints[i][0].x + squareLen / 2)))[j];
			}
		}

	}

	void boundariesCorrection() {
		for (int i = 1; i < SAMPLE_NUM; i++) {
			for (int j = 0; j < 3; j++) {
				cLower[i][j] = cLower[0][j];
				cUpper[i][j] = cUpper[0][j];

				cBackLower[i][j] = cBackLower[0][j];
				cBackUpper[i][j] = cBackUpper[0][j];
			}
		}

		for (int i = 0; i < SAMPLE_NUM; i++) {
			for (int j = 0; j < 3; j++) {
				if (avgColor[i][j] - cLower[i][j] < 0)
					cLower[i][j] = avgColor[i][j];

				if (avgColor[i][j] + cUpper[i][j] > 255)
					cUpper[i][j] = 255 - avgColor[i][j];

				if (avgBackColor[i][j] - cBackLower[i][j] < 0)
					cBackLower[i][j] = avgBackColor[i][j];

				if (avgBackColor[i][j] + cBackUpper[i][j] > 255)
					cBackUpper[i][j] = 255 - avgBackColor[i][j];
			}
		}
	}

	void cropBinImg(Mat imgIn, Mat imgOut) {
		imgIn.copyTo(binTmpMat3);

		Rect boxRect = makeBoundingBox(binTmpMat3);
		Rect finalRect = null;

		if (boxRect != null) {
			Mat roi = new Mat(imgIn, boxRect);
			int armMargin = 2;

			Point tl = boxRect.tl();
			Point br = boxRect.br();

			imgIn.cols();
			int rowNum = imgIn.rows();

			int wristThresh = 10;

			List<Integer> countOnes = new ArrayList<Integer>();

			if (tl.x < armMargin) {
				int localMinId = 0;
				for (int x = (int) tl.x; x < br.x; x++) {
					int curOnes = Core.countNonZero(roi.col(x));
					int lstTail = countOnes.size() - 1;
					if (lstTail >= 0) {
						if (curOnes < countOnes.get(lstTail)) {
							localMinId = x;
						}
					}

					if (curOnes > (countOnes.get(localMinId) + wristThresh))
						break;

					countOnes.add(curOnes);
				}

				Rect newBoxRect = new Rect(new Point(localMinId, tl.y), br);
				roi = new Mat(imgIn, newBoxRect);

				Point newtl = newBoxRect.tl();
				Point newbr = newBoxRect.br();

				int y1 = (int) newBoxRect.tl().y;
				while (Core.countNonZero(roi.row(y1)) < 2) {
					y1++;
				}

				int y2 = (int) newBoxRect.br().y;
				while (Core.countNonZero(roi.row(y2)) < 2) {
					y2--;
				}
				finalRect = new Rect(new Point(newtl.x, y1), new Point(newbr.x, y2));
			} else if (br.y > rowNum - armMargin) {
				int scanCount = 0;
				int scanLength = 8;
				int scanDelta = 8;
				int y;
				for (y = (int) br.y - 1; y > tl.y; y--) {
					int curOnes = Core.countNonZero((roi.row(y - (int) tl.y)));
					int lstTail = countOnes.size() - 1;
					if (lstTail >= 0) {
						countOnes.add(curOnes);

						if (scanCount % scanLength == 0) {
							int curDelta = curOnes - countOnes.get(scanCount - 5);
							if (curDelta > scanDelta)
								break;
						}

					} else
						countOnes.add(curOnes);

					scanCount++;
				}

				finalRect = new Rect(tl, new Point(br.x, y + scanLength));

			}

			if (finalRect != null) {
				roi = new Mat(imgIn, finalRect);
				roi.copyTo(tmpMat);
				imgIn.copyTo(imgOut);
				imgOut.setTo(Scalar.all(0));
				roi = new Mat(imgOut, finalRect);
				tmpMat.copyTo(roi);
			}

		}

	}

	// Generates binary image containing user's hand
	void produceBinImg(Mat imgIn, Mat imgOut) {
		int colNum = imgIn.cols();
		int rowNum = imgIn.rows();
		int boxExtension = 0;

		boundariesCorrection();

		produceBinHandImg(imgIn, binTmpMat);

		produceBinBackImg(imgIn, binTmpMat2);

		Core.bitwise_and(binTmpMat, binTmpMat2, binTmpMat);
		binTmpMat.copyTo(tmpMat);
		binTmpMat.copyTo(imgOut);

		Rect roiRect = makeBoundingBox(tmpMat);

		if (roiRect != null) {
			roiRect.x = Math.max(0, roiRect.x - boxExtension);
			roiRect.y = Math.max(0, roiRect.y - boxExtension);
			roiRect.width = Math.min(roiRect.width + boxExtension, colNum);
			roiRect.height = Math.min(roiRect.height + boxExtension, rowNum);

			Mat roi1 = new Mat(binTmpMat, roiRect);
			Mat roi3 = new Mat(imgOut, roiRect);
			imgOut.setTo(Scalar.all(0));

			roi1.copyTo(roi3);

			Mat element = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
			Imgproc.dilate(roi3, roi3, element, new Point(-1, -1), 2);

			Imgproc.erode(roi3, roi3, element, new Point(-1, -1), 2);

		}

		// cropBinImg(imgOut, imgOut);

	}

	// Generates binary image thresholded only by sampled hand colors
	void produceBinHandImg(Mat imgIn, Mat imgOut) {
		for (int i = 0; i < SAMPLE_NUM; i++) {
			lowerBound.set(new double[] { avgColor[i][0] - cLower[i][0], avgColor[i][1] - cLower[i][1],
					avgColor[i][2] - cLower[i][2] });
			upperBound.set(new double[] { avgColor[i][0] + cUpper[i][0], avgColor[i][1] + cUpper[i][1],
					avgColor[i][2] + cUpper[i][2] });

			Core.inRange(imgIn, lowerBound, upperBound, sampleMats[i]);

		}

		imgOut.release();
		sampleMats[0].copyTo(imgOut);

		for (int i = 1; i < SAMPLE_NUM; i++) {
			Core.add(imgOut, sampleMats[i], imgOut);
		}

		Imgproc.medianBlur(imgOut, imgOut, 3);
	}

	// Generates binary image thresholded only by sampled background colors
	void produceBinBackImg(Mat imgIn, Mat imgOut) {
		for (int i = 0; i < SAMPLE_NUM; i++) {

			lowerBound.set(new double[] { avgBackColor[i][0] - cBackLower[i][0], avgBackColor[i][1] - cBackLower[i][1],
					avgBackColor[i][2] - cBackLower[i][2] });
			upperBound.set(new double[] { avgBackColor[i][0] + cBackUpper[i][0], avgBackColor[i][1] + cBackUpper[i][1],
					avgBackColor[i][2] + cBackUpper[i][2] });

			Core.inRange(imgIn, lowerBound, upperBound, sampleMats[i]);

		}

		imgOut.release();
		sampleMats[0].copyTo(imgOut);

		for (int i = 1; i < SAMPLE_NUM; i++) {
			Core.add(imgOut, sampleMats[i], imgOut);
		}

		Core.bitwise_not(imgOut, imgOut);

		Imgproc.medianBlur(imgOut, imgOut, 7);

	}

	void makeContours() {
		hg.contours.clear();
		Imgproc.findContours(binMat, hg.contours, hg.hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);

		// Find biggest contour and return the index of the contour, which is
		// hg.cMaxId
		hg.findBiggestContour();

		if (hg.cMaxId > -1) {

			hg.approxContour.fromList(hg.contours.get(hg.cMaxId).toList());
			Imgproc.approxPolyDP(hg.approxContour, hg.approxContour, 2, true);
			hg.contours.get(hg.cMaxId).fromList(hg.approxContour.toList());

			// hg.contours.get(hg.cMaxId) represents the contour of the hand
			Imgproc.drawContours(rgbaMat, hg.contours, hg.cMaxId, mColorsRGB[0], 1);

			// Palm center is stored in hg.inCircle, radius of the inscribed
			// circle is stored in hg.inCircleRadius
			hg.findInscribedCircle(rgbaMat);

			hg.boundingRect = Imgproc.boundingRect(hg.contours.get(hg.cMaxId));

			Imgproc.convexHull(hg.contours.get(hg.cMaxId), hg.hullI, false);

			hg.hullP.clear();
			for (int i = 0; i < hg.contours.size(); i++)
				hg.hullP.add(new MatOfPoint());

			int[] cId = hg.hullI.toArray();
			List<Point> lp = new ArrayList<Point>();
			Point[] contourPts = hg.contours.get(hg.cMaxId).toArray();

			for (int i = 0; i < cId.length; i++) {
				lp.add(contourPts[cId[i]]);
				// Core.circle(rgbaMat, contourPts[cId[i]], 2, new Scalar(241,
				// 247, 45), -3);
			}

			// hg.hullP.get(hg.cMaxId) returns the locations of the points in
			// the convex hull of the hand
			hg.hullP.get(hg.cMaxId).fromList(lp);
			lp.clear();

			hg.fingerTips.clear();
			hg.defectPoints.clear();
			hg.defectPointsOrdered.clear();

			hg.fingerTipsOrdered.clear();
			hg.defectIdAfter.clear();

			if ((contourPts.length >= 5) && hg.detectIsHand(rgbaMat) && (cId.length >= 5)) {
				Imgproc.convexityDefects(hg.contours.get(hg.cMaxId), hg.hullI, hg.defects);
				List<Integer> dList = hg.defects.toList();

				for (int i = 0; i < dList.size(); i++) {
					int id = i % 4;
					Point curPoint;

					if (id == 2) { // Defect point
						double depth = (double) dList.get(i + 1) / 256.0;
						curPoint = contourPts[dList.get(i)];

						Point curPoint0 = contourPts[dList.get(i - 2)];
						Point curPoint1 = contourPts[dList.get(i - 1)];
						Point vec0 = new Point(curPoint0.x - curPoint.x, curPoint0.y - curPoint.y);
						Point vec1 = new Point(curPoint1.x - curPoint.x, curPoint1.y - curPoint.y);
						double dot = vec0.x * vec1.x + vec0.y * vec1.y;
						double lenth0 = Math.sqrt(vec0.x * vec0.x + vec0.y * vec0.y);
						double lenth1 = Math.sqrt(vec1.x * vec1.x + vec1.y * vec1.y);
						double cosTheta = dot / (lenth0 * lenth1);

						if ((depth > hg.inCircleRadius * 0.7) && (cosTheta >= -0.7)
								&& (!isClosedToBoundary(curPoint0, rgbaMat))
								&& (!isClosedToBoundary(curPoint1, rgbaMat))) {

							hg.defectIdAfter.add((i));

							Point finVec0 = new Point(curPoint0.x - hg.inCircle.x, curPoint0.y - hg.inCircle.y);
							double finAngle0 = Math.atan2(finVec0.y, finVec0.x);
							Point finVec1 = new Point(curPoint1.x - hg.inCircle.x, curPoint1.y - hg.inCircle.y);
							double finAngle1 = Math.atan2(finVec1.y, finVec1.x);

							if (hg.fingerTipsOrdered.size() == 0) {
								hg.fingerTipsOrdered.put(finAngle0, curPoint0);
								hg.fingerTipsOrdered.put(finAngle1, curPoint1);

							} else {

								hg.fingerTipsOrdered.put(finAngle0, curPoint0);

								hg.fingerTipsOrdered.put(finAngle1, curPoint1);

							}

						}

					}
				}

			}

		}

		if (hg.detectIsHand(rgbaMat)) {

			// hg.boundingRect represents four coordinates of the bounding box.
			Core.rectangle(rgbaMat, hg.boundingRect.tl(), hg.boundingRect.br(), mColorsRGB[1], 2);
			Imgproc.drawContours(rgbaMat, hg.hullP, hg.cMaxId, mColorsRGB[2]);
		}

	}

	boolean isClosedToBoundary(Point pt, Mat img) {
		int margin = 5;
		if ((pt.x > margin) && (pt.y > margin) && (pt.x < img.cols() - margin) && (pt.y < img.rows() - margin)) {
			return false;
		}

		return true;
	}

	Rect makeBoundingBox(Mat img) {
		hg.contours.clear();
		Imgproc.findContours(img, hg.contours, hg.hie, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
		hg.findBiggestContour();

		if (hg.cMaxId > -1) {

			hg.boundingRect = Imgproc.boundingRect(hg.contours.get(hg.cMaxId));

		}

		if (hg.detectIsHand(rgbaMat)) {

			return hg.boundingRect;
		} else
			return null;
	}

	@Override
	public void onPause() {
		Log.i(TAG, "Paused!");
		super.onPause();
		if (mWaveCameraView != null) {
			mWaveCameraView.disableView();
		}
	}

	@Override
	public void onResume() {

		super.onResume();
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);

		
		Log.i(TAG, "Resumed!");
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		if (mWaveCameraView != null) {
			mWaveCameraView.disableView();
		}
		SharedPreferences numbers = getSharedPreferences("Numbers", 0);
		SharedPreferences.Editor editor = numbers.edit();
		editor.putInt("imgNum", imgNum);
		editor.commit();

		Log.i(TAG, "Destroyed!");
	}
}